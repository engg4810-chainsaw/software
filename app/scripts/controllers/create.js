'use strict';

angular.module('engg4810App')
  .controller('CreateCtrl', function ($scope, $timeout, $http, cloudBrowser) {
    var lineColour = '#131540';

    // Map instance on the page.
    $scope.map = null;

    // Contains the route being created by the user
    //  - path: a list of coordinates
    $scope.route = {
      path: []
    };

    var mapCentre = function() {
      // Default coordinates are UQ St Lucia.
      var longitude = 153.0136891;
      var latitude = -27.4985686;

      // TODO: Attempt to get user's current location

      return {longitude: longitude, latitude: latitude};
    };

    $scope.showMap = function() {
      if ($scope.map === null) {
        var coords = mapCentre();
        // Wrap in timeout, to ensure the DOM element is created.
        $timeout(function() {
          $scope.map = new GMaps({
            div: '#map',
            lat: coords.latitude,
            lng: coords.longitude
          });
          initialiseMarkers();
        });
      } else {
        // Resize the map, to ensure it is visible.
        // (necessary for Bootstrap tabs)
        google.maps.event.trigger($scope.map, 'resize');
      }
    };

    // Initialise the display
    $scope.showMap();

    // Initialise the marker placement
    var initialiseMarkers = function() {
      GMaps.on('click', $scope.map.map, function(event) {
        var index = $scope.map.markers.length;
        var lat = event.latLng.lat();
        var lng = event.latLng.lng();

        $scope.map.addMarker({
          lat: lat,
          lng: lng,
          title: 'Marker #' + index,
        });

        $scope.route.path.push({lat: lat, lng: lng, high: false});
        $scope.$apply(); // Ensure the scope is updated (necessary for callbacks)
      });
    };

    $scope.downloadTrackFile = function () {
      $http({
        url: cloudBrowser.getConfigUrl(),
        method: 'POST',
        data: {points: $scope.route.path}
      })
      .success(function(data) {
        // Download the file
        window.location = cloudBrowser.getDownloadTrackConfigUrl(data.fid);
      })
      .error(function(data, status, headers, config) {
        // Failed
        console.log(data, status, headers, config);
      });
    };

    $scope.downloadWomFile = function() {
      // Download the file.
      window.location = cloudBrowser.getDownloadWoMConfigUrl();
    };

    $scope.$watch('route.path', function() {
      if ($scope.map !== null) {
        // Remove the existing paths.
        $scope.map.removePolylines();

        if ($scope.route.path.length > 1) {
          // Redraw the path polyline.
          var path = [];
          for (var i = 0; i < $scope.route.path.length; ++i) {
            var p = $scope.route.path[i];
            path.push([p.lat, p.lng]);
          }

          $scope.map.drawPolyline({
            path: path,
            strokeColor: lineColour,
            strokeOpacity: 0.8,
            strokeWeight: 6
          });
        }
      }
    }, true);

    $scope.removePoint = function(index) {
      // Remove the point
      $scope.route.path.splice(index, 1);

      // Redraw the markers
      $scope.map.removeMarkers();
      for (var i = 0; i < $scope.route.path.length; ++i) {
        var point = $scope.route.path[i];
        $scope.map.addMarker({
          lat: point.lat,
          lng: point.lng,
          title: 'Marker #' + i,
        });
      }
    };

  });
