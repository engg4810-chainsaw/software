'use strict';

angular.module('engg4810App')
  .controller('MainCtrl', function ($scope, $timeout, $modal, cloudBrowser) {
    // Colours for route display on the map.
    var withinThresholdColour = '#131540';
    var outsideThresholdColour = '#ffae00';

    // The data types supported for visualisation.
    $scope.dataTypes = {
      acceleration: 'Acceleration',
      humidity: 'Humidity',
      luminosity: 'Luminosity',
      pressure: 'Pressure',
      sound: 'Sound',
      temperature: 'Temperature',
      uv: 'UV',
    };

    // The configuration for plots.
    $scope.plotOptions = {
      xaxis: {
        mode: 'time'
      }
    };

    // Map instance on the page.
    $scope.map = null;

    // Contains the raw and processed data for the current file.
    //  - path: the group and filename
    //  - raw: the raw data, from the API
    //  - processed: plot data, paths
    //  - types: the types of data available in the file
    $scope.file = {
      path: null,
      raw: null,
      processed: {},
      types: [],
      routeBad: false,
      missingGps: false
    };

    // Thresholds are filled in once available data types are known.
    $scope.threshold = {};

    // Load the groups.
    cloudBrowser.getGroups().success(function(data) {
      if ('error' in data) {
        // Do something
      } else {
        $scope.groups = data.groups;
      }
    });

    $scope.openFile = function () {
      var modalInstance = $modal.open({
        templateUrl: 'views/openFile.html',
        controller: 'OpenFileCtrl',
        resolve: {
          groups: function () {
            return $scope.groups;
          },
          cloudBrowser: function() {
            return cloudBrowser;
          }
        }
      });

      modalInstance.result.then(function (selected) {
        $scope.file.path = selected;
      });
    };

    $scope.$watch('file.path', function(newFile) {
      if (newFile !== null) {
        cloudBrowser.getData(newFile.group, newFile.fileURI)
          .success(function(data) {
            $scope.file.raw = data;
          });
      }
    });

    $scope.$watch('file.raw', function() {
      $scope.map = null;
      $scope.file.types = [];
      $scope.file.processed = {};
      $scope.showMap();

      if ($scope.file.raw === null || $scope.file.raw.hasOwnProperty('error')) {
        // Do not attempt to load file.
        return;
      }

      var types = getDataTypes();
      updateThresholds(types);
      // Update the paths and show the new route.
      findPaths();
      showRoute();
      updatePlotData(types);
      $scope.file.types = types;
    });

    var getValue = function(sample, type) {
      var v = sample[type];
      if (type === 'acceleration') {
        return Math.sqrt(
          Math.pow(v[0], 2) + Math.pow(v[1], 2) + Math.pow(v[2], 2));
      }
      return v;
    };

    var mapCentre = function() {
      // Default coordinates are UQ St Lucia.
      var longitude = 153.0136891;
      var latitude = -27.4985686;

      if ($scope.file.raw !== null && $scope.file.raw.hasOwnProperty('samples')) {
        // Search for the first point with latitude and longitude.
        for (var i = 0; i < $scope.file.raw.samples.length; ++i) {
          var sample = $scope.file.raw.samples[i];
          if (sample.hasOwnProperty('latitude') &&
              sample.hasOwnProperty('longitude')) {
            longitude = sample.longitude;
            latitude = sample.latitude;
            break;
          }
        }
      }

      return {longitude: longitude, latitude: latitude};
    };

    var findPaths = function(types) {
      if (typeof types === 'undefined') {
        types = $scope.file.types;
      }

      var withinThresholdPaths = [];
      var outsideThresholdPaths = [];
      var missingGps = false;

      if ($scope.file.raw !== null) {
        var samples = $scope.file.raw.samples;
        var tempPath = [];
        var prevBad = null;

        for (var i = 0; i < samples.length; ++i) {
          var sample = samples[i];
          var bad = false;
          if (sample.hasOwnProperty('latitude') &&
              sample.hasOwnProperty('longitude')) {
            // Determine if point is within bounds.
            for (var j = 0; j < types.length; ++j) {
              var type = types[j];
              var val = getValue(sample, type);
              if (val < $scope.threshold[type].low ||
                  val > $scope.threshold[type].high) {
                bad = true;
                break;
              }
            }

            tempPath.push([sample.latitude, sample.longitude]);

            // Determine if a new path should be created,
            // based on the status of the point.
            if (prevBad !== null && bad !== prevBad) {
              if (prevBad) {
                // Add path to problem paths list.
                outsideThresholdPaths.push(tempPath);
              } else {
                // Add path to good paths list.
                withinThresholdPaths.push(tempPath);
              }

              tempPath = [[sample.latitude, sample.longitude]];
            }

            prevBad = bad;
          } else {
            // Route is missing a GPS location for a data point.
            missingGps = true;
          }
        }

        if (prevBad) {
          // Add final path to problem paths list.
          outsideThresholdPaths.push(tempPath);
        } else {
          // Add final path to good paths list.
          withinThresholdPaths.push(tempPath);
        }
      }

      $scope.file.processed.withinThresholdPaths = withinThresholdPaths;
      $scope.file.processed.outsideThresholdPaths = outsideThresholdPaths;
      $scope.file.routeBad = outsideThresholdPaths.length > 0;
      $scope.file.missingGps = missingGps;
    };

    var showRoute = function() {
      $timeout(function() {
        var d = $scope.file.processed;

        if ($scope.map !== null &&
            d.hasOwnProperty('withinThresholdPaths') &&
            d.hasOwnProperty('outsideThresholdPaths')) {

          // Remove the existing paths.
          $scope.map.removePolylines();

          // Add the good paths.
          for (var i = 0; i < d.withinThresholdPaths.length; ++i) {
            $scope.map.drawPolyline({
              path: d.withinThresholdPaths[i],
              strokeColor: withinThresholdColour,
              strokeOpacity: 0.8,
              strokeWeight: 6
            });
          }

          // Add the problem paths.
          for (var j = 0; j < d.outsideThresholdPaths.length; ++j) {
            $scope.map.drawPolyline({
              path: d.outsideThresholdPaths[j],
              strokeColor: outsideThresholdColour,
              strokeOpacity: 0.8,
              strokeWeight: 6
            });
          }
        }
      });
    };

    $scope.showMap = function() {
      if ($scope.map === null) {
        var coords = mapCentre();
        // Wrap in timeout, to ensure the DOM element is created.
        $timeout(function() {
          $scope.map = new GMaps({
            div: '#map',
            lat: coords.latitude,
            lng: coords.longitude
          });
        });
      } else {
        // Resize the map, to ensure it is visible.
        // (necessary for Bootstrap tabs)
        google.maps.event.trigger($scope.map, 'resize');
      }
    };

    var getDataTypes = function() {
      var available = [];
      if ($scope.file.raw !== null) {
        var samples = $scope.file.raw.samples;

        for (var type in $scope.dataTypes) {
          // Check for the type in one of the samples.
          for (var i = 0; i < samples.length; ++i) {
            if (samples[i].hasOwnProperty(type)) {
              available.push(type);
              break;
            }
          }
        }
      }
      return available;
    };

    var getPlotData = function(type) {
      var data = [];
      if ($scope.file.raw !== null) {
        var samples = $scope.file.raw.samples;

        for (var i = 0; i < samples.length; ++i) {
          if (samples[i][type] !== undefined) {
            var date = new Date(samples[i].time).getTime();
            var val = getValue(samples[i], type);
            data.push([date, val]);
          }
        }
      }

      $scope.file.processed.plotData[type] = [data];
    };

    var updatePlotData = function(types) {
      $scope.file.processed.plotData = {};
      for (var i = 0; i < types.length; ++i) {
        getPlotData(types[i]);
      }
    };

    /* Thresholds */
    var thresholdDefaults = {
      acceleration: {
        low: 0,
        high: 5,
        min: -5,
        max: 10,
        adv: false
      },
      humidity: {
        low: 0,
        high: 50,
        min: -10,
        max: 100,
        adv: false
      },
      luminosity: {
        low: 0,
        high: 500,
        min: -10,
        max: 3000,
        adv: false
      },
      pressure: {
        low: 80,
        high: 110,
        min: 0,
        max: 150,
        adv: false
      },
      sound: {
        low: 0,
        high: 50,
        min: -10,
        max: 100,
        adv: false
      },
      temperature: {
        low: 0,
        high: 30,
        min: -30,
        max: 100,
        adv: false
      },
      uv: {
        low: 0,
        high: 50,
        min: -10,
        max: 100,
        adv: false
      }
    };

    var updateThresholds = function(types) {
      $scope.threshold = {};
      for (var i = 0; i < types.length; ++i) {
        var type = types[i];
        $scope.threshold[type] = angular.copy(thresholdDefaults[type]);
      }
    };

    $scope.$watch('threshold', function() {
      // Thresholds changed, so update the map route.
      findPaths();
      showRoute();
    }, true);

    $scope.uploadFile = function () {
      var modalInstance = $modal.open({
        templateUrl: 'views/upload.html',
        controller: 'UploadFileCtrl',
        resolve: {
          cloudBrowser: function() {
            return cloudBrowser;
          }
        }
      });

      modalInstance.result.then(function () {
        // Display message to indicate upload was successful.
      });
    };

  })


  .controller('OpenFileCtrl', function ($scope, $modalInstance, groups, cloudBrowser) {

    $scope.treeOptions = {
      nodeChildren: 'children',
      dirSelectable: false
    };

    $scope.groups = groups;
    $scope.dirTree = [];
    $scope.selected = {
      group: '',
      file: ''
    };

    $scope.$watch('selected.group', function() {
      if ($scope.selected.group !== '') {
        cloudBrowser.getDirTree($scope.selected.group).success(function(data) {
          if ('error' in data) {
            // Do something
          } else {
            $scope.dirTree = data;
          }
        });
      }
    });

    $scope.ok = function () {
      $modalInstance.close({
        group: $scope.selected.group,
        file: $scope.selected.file.path,
        fileURI: encodeURI($scope.selected.file.path)
      });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

  })


  .controller('UploadFileCtrl', function ($scope, $modalInstance, $upload, cloudBrowser) {

    var groupPrefix = 'engg4810g';
    $scope.treeOptions = {
      nodeChildren: 'children',
      dirSelectable: true,
      isLeaf: function() {
        // Only lists directories, so no leaf.
        return false;
      }
    };

    $scope.groups = [];
    $scope.files = [];
    $scope.dirTree = [];
    $scope.metadata = {
      group: '',
      dir: '',
      filename: '',
      comment: '',
      team: '',
      config: ''
    };
    $scope.filename = '';

    // Load the groups.
    cloudBrowser.getStorageGroups().success(function(data) {
      if ('error' in data) {
        // Do something
      } else {
        $scope.groups = data.groups;
      }
    });

    $scope.$watch('metadata.group', function() {
      if ($scope.metadata.group !== '') {
        var team = $scope.metadata.group.split(groupPrefix)[1];
        $scope.metadata.team = parseInt(team, 10);

        cloudBrowser.getDirOnlyTree($scope.metadata.group).success(function(data) {
          if ('error' in data) {
            // Do something
          } else {
            $scope.dirTree = data;
          }
        });
      }
    });

    $scope.onFileSelect = function($files) {
      //$files: an array of files selected, each file has name, size, and type.
      $scope.files = $files;
      $scope.filename = $files[0].name;
      // Remove the file extension before populating the filename textbox.
      $scope.metadata.filename = $scope.filename.replace(/\.[^/.]+$/, '');
    };

    $scope.ok = function () {
      upload();
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    var updateProgress = function(evt) {
      console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
    };

    var success = function(data, status, headers, config) {
      // file is uploaded successfully
      console.log(data, status, headers, config);
      $modalInstance.close();
    };

    var upload = function() {
      for (var i = 0; i < $scope.files.length; i++) {
        var file = $scope.files[i];
        $scope.upload = $upload.upload({
          url: cloudBrowser.getUploadUrl(), //upload.php script, node.js route, or servlet url
          method: 'POST', // or 'PUT',
          // headers: {'header-key': 'header-value'},
          // withCredentials: true,
          data: {
            group: $scope.metadata.group,
            dir: $scope.metadata.dir,
            comment: $scope.metadata.comment,
            team: parseInt($scope.metadata.team, 10),
            config: $scope.metadata.config,
            filename: $scope.metadata.filename,
          },
          file: file, // or list of files: $files for html5 only
          /* set the file formData name ('Content-Desposition'). Default is 'file' */
          //fileFormDataName: myFile, //or a list of names for multiple files (html5).
          /* customize how data is added to formData. See #40#issuecomment-28612000 for sample code */
          //formDataAppender: function(formData, key, val){}
        })
        .progress(updateProgress)
        .success(success);
        //.error(...)
        //.then(success, error, progress);
        //.xhr(function(xhr){xhr.upload.addEventListener(...)})// access and attach any event listener to XMLHttpRequest.
      }
      /* alternative way of uploading, send the file binary with the file's content-type.
         Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed.
         It could also be used to monitor the progress of a normal http post/put request with large data*/
      // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
    };

  });
