'use strict';

angular.module('engg4810App')
  .controller('MultipleCtrl', function ($scope, $timeout, $modal, cloudBrowser) {
    var modeAvg = 0;
    var modeMin = 1;

    // Grid size is 100x100
    var gridSize = 100;

    // The data types supported for visualisation.
    $scope.dataTypes = {
      acceleration: 'Acceleration',
      humidity: 'Humidity',
      luminosity: 'Luminosity',
      pressure: 'Pressure',
      sound: 'Sound',
      temperature: 'Temperature',
      uv: 'UV',
    };

    $scope.mode = modeAvg;
    $scope.folder = {
      group: null,
      path: null,
    };
    $scope.data = null;
    $scope.types = [];

    // Map instance on the page.
    $scope.map = null;
    $scope.heatmap = null;

    // Thresholds are filled in once available data types are known.
    $scope.threshold = {};

    var mapCentre = function() {
      // Default coordinates are UQ St Lucia.
      var longitude = 153.0136891;
      var latitude = -27.4985686;

      // TODO: Attempt to get user's current location

      return {longitude: longitude, latitude: latitude};
    };

    // Only recalculate the heatmap data once the event has finished.
    // Performance is decreased if the data is processed continuously
    // throughout the event.
    var recalculateData = false;
    var mapChanged = function() {
      recalculateData = true;
    };

    var doMapChange = function() {
      processData();
      recalculateData = false;
    };

    $scope.showMap = function() {
      if ($scope.map === null) {
        var coords = mapCentre();
        // Wrap in timeout, to ensure the DOM element is created.
        $timeout(function() {
          $scope.map = new GMaps({
            div: '#map',
            lat: coords.latitude,
            lng: coords.longitude
          });

          // Recalculate heatmap data when map is panned, zoomed or resized.
          google.maps.event.addListener($scope.map.map, 'zoom_changed', mapChanged);
          google.maps.event.addListener($scope.map.map, 'center_changed', mapChanged);
          google.maps.event.addListener($scope.map.map, 'bounds_changed', mapChanged);
          google.maps.event.addListener($scope.map.map, 'idle', doMapChange);
        });
      } else {
        // Resize the map, to ensure it is visible.
        // (necessary for Bootstrap tabs)
        google.maps.event.trigger($scope.map, 'resize');
      }
    };

    // Initialise the display
    $scope.showMap();

    $scope.openDir = function () {
      var modalInstance = $modal.open({
        templateUrl: 'views/openDir.html',
        controller: 'OpenDirCtrl',
        resolve: {
          cloudBrowser: function() {
            return cloudBrowser;
          }
        }
      });

      modalInstance.result.then(function (selected) {
        $scope.folder = selected;
      });
    };

    $scope.$watch('folder', function(newDir) {
      if (newDir.group !== null) {
        cloudBrowser.getDirData(newDir.group, newDir.path)
          .success(function(data) {
            $scope.data = data;
          });
      }
    }, true);

    $scope.$watch('data', function() {
      var types = getDataTypes();
      updateThresholds(types);
      processData();
      $scope.types = types;
    });

    var getDataTypes = function() {
      var available = [];
      if ($scope.data !== null) {
        var samples = $scope.data.samples;

        for (var type in $scope.dataTypes) {
          // Check for the type in one of the samples.
          for (var i = 0; i < samples.length; ++i) {
            if (samples[i].hasOwnProperty(type)) {
              available.push(type);
              break;
            }
          }
        }
      }
      return available;
    };

    var getValue = function(sample, type) {
      var v = sample[type];
      if (type === 'acceleration') {
        return Math.sqrt(
          Math.pow(v[0], 2) + Math.pow(v[1], 2) + Math.pow(v[2], 2));
      }
      return v;
    };

    var processData = function() {
      if ($scope.map === null) {
        // Map does not exist, so do not attempt to generate heatmap.
        return;
      }

      var bounds = $scope.map.map.getBounds();

      if (!bounds.isEmpty() && $scope.data !== null && $scope.data.samples.length > 0) {
        var grid = {};
        var i, j, k; // indexes

        // Size of each grid box.
        var ne = bounds.getNorthEast();
        var sw = bounds.getSouthWest();
        var latGridSize = (ne.lat() - sw.lat()) / gridSize;
        var lngGridSize = (ne.lng() - sw.lng()) / gridSize;

        // Construct the grid, based on the gridSize. i is lat, j is lng
        for (i = 0; i < gridSize; ++i) {
          grid[i] = {};
          // Calculate latitudes for all boxes in this row.
          var neLat = ne.lat() - (i * latGridSize);
          var swLat = ne.lat() - ((i + 1) * latGridSize);

          for (j = 0; j < gridSize; ++j) {
            // Calculate longitude for this box.
            var neLng = sw.lng() + ((j + 1) * lngGridSize);
            var swLng = sw.lng() + (j * lngGridSize);

            // Create points for north-east and south-west corners of box.
            var nePnt = new google.maps.LatLng(neLat, neLng);
            var swPnt = new google.maps.LatLng(swLat, swLng);

            // Create bounding box for this grid box, to get centre point.
            var box = new google.maps.LatLngBounds(swPnt, nePnt);
            grid[i][j] = {'values': [], 'center': box.getCenter()};
          }
        }

        // Calculate the data for the heatmap
        for (k = 0; k < $scope.data.samples.length; ++k) {
          var s = $scope.data.samples[k];
          if (s.hasOwnProperty('latitude') &&
              s.hasOwnProperty('longitude')) {
            // Use the data from the sample, as it has a latitude and longitude.
            var pt = new google.maps.LatLng(s.latitude, s.longitude);
            if (bounds.contains(pt)) {
              // Find box for the data
              var latIndex = Math.abs(Math.floor((s.latitude - ne.lat()) / latGridSize));
              var lngIndex = Math.abs(Math.floor((s.longitude - sw.lng()) / lngGridSize));

              // Add data to box containing point.
              for (var t = 0; t < $scope.types.length; ++t) {
                var type = $scope.types[t];
                var val = getValue(s, type);

                var diff = (val / $scope.threshold[type].high) * 100;
                if (diff > 100) {
                  diff = 100;
                } else if (diff < 0) {
                  diff = 0;
                }

                grid[latIndex][lngIndex].values.push(diff);
              }
            }
          }
        }

        // Generate the heatmap config.
        var heatmapData = [];
        for (i = 0; i < gridSize; ++i) {
          for (j = 0; j < gridSize; ++j) {
            var value = 0, boxData = grid[i][j];
            if ($scope.mode === modeAvg) {
              // Average of all data for the grid box.
              var sum = 0;
              for (k = 0; k < boxData.values.length; ++k) {
                sum += boxData.values[k];
              }
              value = sum / boxData.values.length;
            } else if ($scope.mode === modeMin) {
              // Minimum of all data for the grid box.
              if (boxData.values.length) {
                value = Math.min.apply(Math, boxData.values);
              }
            }

            value = value || 0; // Eliminate NaNs.

            if (value) {
              heatmapData.push({
                location: boxData.center,
                weight: value
              });
            }
          }
        }

        // Apply the heatmap
        showHeatmap(heatmapData);
      }
    };

    var showHeatmap = function(data) {
      if ($scope.heatmap === null) {
        $scope.heatmap = new google.maps.visualization.HeatmapLayer({
          data: data,
          gradient: gradient
        });
      } else {
        $scope.heatmap.setData(data);
      }

      $scope.heatmap.setMap($scope.map.map);
    };

    /* Thresholds */
    var thresholdDefaults = {
      acceleration: {
        low: 0,
        high: 5,
        min: -5,
        max: 10,
        adv: false
      },
      humidity: {
        low: 0,
        high: 50,
        min: -10,
        max: 100,
        adv: false
      },
      luminosity: {
        low: 0,
        high: 500,
        min: -10,
        max: 3000,
        adv: false
      },
      pressure: {
        low: 80,
        high: 110,
        min: 0,
        max: 150,
        adv: false
      },
      sound: {
        low: 0,
        high: 50,
        min: -10,
        max: 100,
        adv: false
      },
      temperature: {
        low: 0,
        high: 30,
        min: -30,
        max: 100,
        adv: false
      },
      uv: {
        low: 0,
        high: 50,
        min: -10,
        max: 100,
        adv: false
      }
    };

    var updateThresholds = function(types) {
      $scope.threshold = {};
      for (var i = 0; i < types.length; ++i) {
        var type = types[i];
        $scope.threshold[type] = angular.copy(thresholdDefaults[type]);
      }
    };

    // Update heatmap when threshold changes.
    $scope.$watch('threshold', processData, true);

    // Update heatmap when mode changes.
    $scope.$watch('mode', processData);

    var generateGradient = function() {
      var colours = ['rgba(0, 0, 255, 1)'];

      // Blue to Green
      for (var i = 1; i <= 255; i += 2) {
        colours.push('rgba(0, ' + i + ', ' + (255-i) + ', 1)');
      }

      // Green to Red
      for (var j = 1; j <= 255; j += 2) {
        colours.push('rgba(' + j + ', ' + (255-j) + ', 0, 1)');
      }

      return colours;
    };

    // List of colours to use for heatmap, with first element being low value.
    var gradient = generateGradient();

  })


  .controller('OpenDirCtrl', function ($scope, $modalInstance, $upload, cloudBrowser) {

    $scope.treeOptions = {
      nodeChildren: 'children',
      dirSelectable: true,
      isLeaf: function() {
        // Only lists directories, so no leaf.
        return false;
      }
    };

    $scope.groups = [];
    $scope.dirTree = [];
    $scope.selected = {
      group: '',
      dir: ''
    };

    // Load the groups.
    cloudBrowser.getGroups().success(function(data) {
      if ('error' in data) {
        // Do something
      } else {
        $scope.groups = data.groups;
      }
    });

    $scope.$watch('selected.group', function() {
      if ($scope.selected.group !== '') {
        cloudBrowser.getDirOnlyTree($scope.selected.group).success(function(data) {
          if ('error' in data) {
            // Do something
          } else {
            $scope.dirTree = data;
          }
        });
      }
    });

    $scope.ok = function () {
      $modalInstance.close({
        group: $scope.selected.group,
        path: $scope.selected.dir.path,
      });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

  });
