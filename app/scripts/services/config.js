'use strict';

angular.module('services.config', [])
  .constant('configuration', {
    apiUrl: 'http://localhost:8080/'
  });
