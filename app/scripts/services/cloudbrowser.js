'use strict';

angular.module('engg4810App')
  .factory('cloudBrowser', function ($http, configuration) {
    // Service logic

    var currentGroup = '';

    // Public API here
    return {
      getCurrentGroup: function () {
        return currentGroup;
      },

      setCurrentGroup: function(group) {
        currentGroup = group;
      },

      getGroups: function () {
        var url = configuration.apiUrl + 'groups';
        return $http.get(url);
      },

      getFiles: function(group) {
        if (typeof group === undefined) {
          group = currentGroup;
        }

        var url = configuration.apiUrl + 'files/' + group;
        return $http.get(url);
      },

      getData: function(group, filename) {
        var url = configuration.apiUrl + 'data/' + group + '/' + filename;
        return $http.get(url);
      },

      getDirData: function(group, dirPath) {
        var url = configuration.apiUrl + 'dir_data/' + group;
        if (dirPath !== '') {
          url += '/' + dirPath;
        }
        return $http.get(url);
      },

      getStorageGroups: function() {
        var url = configuration.apiUrl + 'storage_groups';
        return $http.get(url);
      },

      getDirTree: function(group) {
        var url = configuration.apiUrl + 'dir_tree';
        if (typeof group !== undefined) {
          url += '/' + group;
        }
        return $http.get(url);
      },

      getDirOnlyTree: function(group) {
        var url = configuration.apiUrl + 'dir_only_tree';
        if (typeof group !== undefined) {
          url += '/' + group;
        }
        return $http.get(url);
      },

      getConfigUrl: function() {
        return configuration.apiUrl + 'config';
      },

      getDownloadTrackConfigUrl: function(filename) {
        return configuration.apiUrl + 'download_config' + '/' + filename;
      },

      getDownloadWoMConfigUrl: function() {
        return configuration.apiUrl + 'download_wom_config';
      },

      getUploadUrl: function() {
        return configuration.apiUrl + 'upload';
      }
    };
  });
