'use strict';

/*
 * Angular Flot directive.
 * Based on https://github.com/develersrl/angular-flot
 *
 * Modified by Joel Addison, 2014
 *   - Add responsive callbacks, to make flot charts work properly in
 *     Bootstrap tabs
 */


angular.module('engg4810App')
  .directive('flot', function($window) {
    return {
      restrict: 'E',
      template: '<div></div>',
      scope: {
        dataset: '=',
        options: '='
      },
      link: function(scope, element, attributes) {
        var height, init, onDatasetChanged, onOptionsChanged, plotArea, width;
        width = attributes.width || '100%';
        height = attributes.height || '100%';
        if (!scope.dataset) {
          scope.dataset = [];
        }
        if (!scope.options) {
          scope.options = {
            legend: {
              show: false
            }
          };
        }

        var plot;

        plotArea = $(element.children()[0]);
        plotArea.css({
          width: width,
          height: height
        });
        init = function() {
          plot = $.plot(plotArea, scope.dataset, scope.options);
          return plot;
        };
        onDatasetChanged = function(dataset) {
          if (plot) {
            plot.setData(dataset); // Load new data
            plot.setupGrid();  // Fix up grid, if required
            return plot.draw();  // Redraw the plot with new data
          } else {
            return init();
          }
        };
        scope.$watch('dataset', onDatasetChanged, true);

        scope.getElementDimensions = function () {
          return { 'h': element.height(), 'w': element.width() };
        };

        scope.$watch(scope.getElementDimensions, function () {
          init();
        }, true);

        angular.element($window).bind('resize', function() {
          init();
        });

        onOptionsChanged = function() {
          return init();
        };
        return scope.$watch('options', onOptionsChanged, true);
      }
    };
  });
