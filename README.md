Chainsaw
========

A web-based application for visualising journeys taken by the portable logging
device.


## Installation

### Development

During development, a number of frameworks are used to reduce the complexity
of managing dependencies and other tasks. You will need to install:

* NodeJS (http://nodejs.org)
* Bower (http://bower.io)
* Yeoman (http://yeoman.io/)
* Yeoman Angular Generator (https://github.com/yeoman/generator-angular)
* Grunt (http://gruntjs.com/)

After you have installed the above tools, you are ready to setup the 
dependencies for the application:

    $ npm install
    $ bower install

To start testing, use the provided `grunt` task to start the development
web server. This will compile all JavaScript and SCSS/Compass on the fly,
and perform `bower` installs when necessary. LiveReload is also configured
for the developemt task, which allows pages in the web browser to be 
automatically refreshed on each change.

    $ grunt serve


### Deployment

The application can be deployed to any web server. No server-side languages are
required.

To build a new release, use the provided `grunt` task.

    $ grunt build

Following the build process, you can save the release to a location of your
choice. Provided with the application is a local build destination, which
will commit the latest build to the `build` branch of the repository.

    $ grund buildcontrol:local

Deployment is as simple as checking out the latest version of the `build` 
branch, and placing it in a location accessible to a web server.
