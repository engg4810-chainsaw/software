'use strict';

describe('Service: cloudBrowser', function () {

  // load the service's module
  beforeEach(module('engg4810App'));

  // instantiate service
  var cloudBrowser;
  beforeEach(inject(function (_cloudBrowser_) {
    cloudBrowser = _cloudBrowser_;
  }));

  it('should do something', function () {
    expect(!!cloudBrowser).toBe(true);
  });

});
