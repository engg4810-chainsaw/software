'use strict';

describe('Controller: MultipleCtrl', function () {

  // load the controller's module
  beforeEach(module('engg4810App'));

  var MultipleCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MultipleCtrl = $controller('MultipleCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
